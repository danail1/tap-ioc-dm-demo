package com.infinitelambda.di.constructor;

import com.infinitelambda.base.Album;
import com.infinitelambda.base.AlbumFinder;
import com.infinitelambda.di.AlbumLoader;

import java.util.List;

// This is my client
public class ConstructorDIAlbumFinder implements AlbumFinder {

    private final AlbumLoader albumLoader;

    public ConstructorDIAlbumFinder(AlbumLoader albumLoader) {
        this.albumLoader = albumLoader;
    }

    @Override
    public Album findAlbumByTitle(String title) {
        final List<Album> allAlbums = albumLoader.findAllAlbums();
        if (allAlbums == null) {
            return null;
        }

        return allAlbums.stream().filter(album -> album.getTitle().equals(title)).findFirst().orElse(null);
    }
}
