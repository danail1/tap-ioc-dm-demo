package com.infinitelambda.di.setter;

import com.infinitelambda.base.Album;
import com.infinitelambda.di.AlbumLoader;

import java.util.List;

public class SetterDIAlbumFinder implements AlbumLoader {

    private AlbumLoader albumLoader;

    public void setAlbumLoader(AlbumLoader albumLoader) {
        this.albumLoader = albumLoader;
    }

    @Override
    public List<Album> findAllAlbums() {
        // This class just showcases setter injection
        return null;
    }
}
