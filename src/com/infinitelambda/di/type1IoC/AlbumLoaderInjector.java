package com.infinitelambda.di.type1IoC;

public interface AlbumLoaderInjector {

    void injectAlbumLoader(AlbumLoaderInjectable injectable);

}
