package com.infinitelambda.di.type1IoC;

import com.infinitelambda.di.AlbumLoader;

public interface AlbumLoaderInjectable {

    void injectAlbumLoader(AlbumLoader albumLoader);

}
