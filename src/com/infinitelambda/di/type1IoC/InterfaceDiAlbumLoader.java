package com.infinitelambda.di.type1IoC;

import com.infinitelambda.base.Album;
import com.infinitelambda.di.AlbumLoader;

import java.util.List;

public class InterfaceDiAlbumLoader implements AlbumLoader, AlbumLoaderInjector {

    @Override
    public List<Album> findAllAlbums() {
        return null;
    }

    @Override
    public void injectAlbumLoader(AlbumLoaderInjectable injectable) {
        injectable.injectAlbumLoader(this);
    }
}
