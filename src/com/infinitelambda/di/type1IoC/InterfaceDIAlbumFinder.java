package com.infinitelambda.di.type1IoC;

import com.infinitelambda.base.Album;
import com.infinitelambda.base.AlbumFinder;
import com.infinitelambda.di.AlbumLoader;

public class InterfaceDIAlbumFinder implements AlbumFinder, AlbumLoaderInjectable {

    private AlbumLoader albumLoader;

    @Override
    public Album findAlbumByTitle(String title) {
        return null;
    }

    @Override
    public void injectAlbumLoader(AlbumLoader albumLoader) {
        this.albumLoader = albumLoader;
    }
}
