package com.infinitelambda.servicelocator;

import com.infinitelambda.base.Album;
import com.infinitelambda.base.AlbumFinder;

import java.util.List;

public class ServiceLocatorAlbumFinder implements AlbumFinder {

    private final LoaderLocator loaderLocator;

    public ServiceLocatorAlbumFinder(ServiceLocator loaderLocator) {
        this.loaderLocator = loaderLocator;
    }

    @Override
    public Album findAlbumByTitle(String title) {
        final AlbumLoader loader = loaderLocator.getAlbumLoader();
        final List<Album> allAlbums = loader.findAllAlbums();
        return null;
    }

}
