package com.infinitelambda.servicelocator;

import java.util.HashMap;
import java.util.Map;

public class DynamicServiceLocator {

    private static final DynamicServiceLocator instance = new DynamicServiceLocator();

    private static final Map<Class<?>, Object> services = new HashMap<>();

    public static DynamicServiceLocator getInstance() {
        return instance;
    }

    private DynamicServiceLocator() {
        services.put(AlbumLoader.class, (AlbumLoader) () -> null);
    }

    public Object getService(Class<?> serviceClass) {
        return services.get(serviceClass);
    }
}
