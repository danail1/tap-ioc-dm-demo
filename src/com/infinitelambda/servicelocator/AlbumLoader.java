package com.infinitelambda.servicelocator;

import com.infinitelambda.base.Album;

import java.util.List;

public interface AlbumLoader {

    List<Album> findAllAlbums();

}
