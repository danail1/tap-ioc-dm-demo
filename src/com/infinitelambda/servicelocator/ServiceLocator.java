package com.infinitelambda.servicelocator;

import com.infinitelambda.base.AlbumFinder;

public class ServiceLocator implements FinderLocator, LoaderLocator {

    private static final ServiceLocator instance = new ServiceLocator();

    public static FinderLocator getFinderLocator() {
        return instance;
    }

    public static LoaderLocator getLoaderLocator() {
        return instance;
    }

    private ServiceLocator() {

    }

    @Override
    public AlbumLoader getAlbumLoader() {
        return () -> null;
    }

    @Override
    public AlbumFinder getAlbumFinder() {
        return new ServiceLocatorAlbumFinder(this);
    }
}
