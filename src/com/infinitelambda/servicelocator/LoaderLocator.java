package com.infinitelambda.servicelocator;

public interface LoaderLocator {

    AlbumLoader getAlbumLoader();

}
