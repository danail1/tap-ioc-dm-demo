package com.infinitelambda.servicelocator;

import com.infinitelambda.base.AlbumFinder;

public interface FinderLocator {

    AlbumFinder getAlbumFinder();

}
