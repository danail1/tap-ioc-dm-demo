package com.infinitelambda.servicelocator;

import com.infinitelambda.base.Album;
import com.infinitelambda.base.AlbumFinder;

public class ServiceLocatorMain {

    public static void main(String[] args) {
        final AlbumFinder finder = ServiceLocator.getFinderLocator().getAlbumFinder();
        final Album album = finder.findAlbumByTitle("Final Countdown");
    }

}
