package com.infinitelambda.strategy;

import com.infinitelambda.base.Album;

import java.util.List;

public class TextFileAlbumLoadingStrategy implements AlbumLoadingStrategy {

    @Override
    public List<Album> findAllAlbums() {
        // TODO: Load albums from a text file
        return null;
    }

}
