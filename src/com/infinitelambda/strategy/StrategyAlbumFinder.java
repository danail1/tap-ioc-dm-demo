package com.infinitelambda.strategy;

import com.infinitelambda.base.Album;
import com.infinitelambda.base.AlbumFinder;

import java.util.List;

// The Context in the Strategy pattern
public class StrategyAlbumFinder implements AlbumFinder {

    private final AlbumLoadingStrategy albumLoadingStrategy;

    public StrategyAlbumFinder(AlbumLoadingStrategy albumLoadingStrategy) {
        this.albumLoadingStrategy = albumLoadingStrategy;
    }

    @Override
    public Album findAlbumByTitle(String title) {
        final List<Album> allAlbums = albumLoadingStrategy.findAllAlbums();
        if (allAlbums == null) {
            return null;
        }

        return allAlbums.stream().filter(album -> album.getTitle().equals(title)).findFirst().orElse(null);
    }

}
