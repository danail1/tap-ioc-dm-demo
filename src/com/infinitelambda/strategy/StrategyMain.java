package com.infinitelambda.strategy;

import com.infinitelambda.base.Album;

public class StrategyMain {

    public static void main(String[] args) {
        final AlbumLoadingStrategy albumLoadingStrategy = new TextFileAlbumLoadingStrategy();
        final StrategyAlbumFinder finder = new StrategyAlbumFinder(albumLoadingStrategy);
        final Album album = finder.findAlbumByTitle("Led Zeppelin IV");
        System.out.println(album);
    }
}
