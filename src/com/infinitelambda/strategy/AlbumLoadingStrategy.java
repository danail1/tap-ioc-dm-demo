package com.infinitelambda.strategy;

import com.infinitelambda.base.Album;

import java.util.List;

// The stragety interface
public interface AlbumLoadingStrategy {

    List<Album> findAllAlbums();

}
