package com.infinitelambda.base;

public interface AlbumFinder {

    Album findAlbumByTitle(String title);

}
