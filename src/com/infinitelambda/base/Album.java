package com.infinitelambda.base;

import java.util.Objects;

public class Album {

    private final String artist;
    private final String title;

    public Album(String artist, String title) {
        this.artist = artist;
        this.title = title;
    }

    public String getArtist() {
        return artist;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Album album = (Album) o;
        return artist.equals(album.artist) && title.equals(album.title);
    }

    @Override
    public int hashCode() {
        return Objects.hash(artist, title);
    }
}
