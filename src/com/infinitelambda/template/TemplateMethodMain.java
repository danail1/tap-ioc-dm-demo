package com.infinitelambda.template;

import com.infinitelambda.base.Album;

public class TemplateMethodMain {

    public static void main(String[] args) {
        final TemplateMethodAlbumFinder finder = new TextFileAlbumFinder();
        final Album album = finder.findAlbumByTitle("Led Zeppelin IV");
        System.out.println(album);
    }

}
