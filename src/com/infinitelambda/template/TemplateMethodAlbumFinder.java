package com.infinitelambda.template;

import com.infinitelambda.base.Album;
import com.infinitelambda.base.AlbumFinder;

import java.util.List;

public abstract class TemplateMethodAlbumFinder implements AlbumFinder {

    @Override
    public Album findAlbumByTitle(String title) {
        final List<Album> allAlbums = findAllAlbums();
        return allAlbums.stream().filter(album -> album.getTitle().equals(title)).findFirst().orElse(null);
    }

    protected abstract List<Album> findAllAlbums();

}
